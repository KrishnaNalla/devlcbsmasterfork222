/************************************
Name         : LCBSManageUsersController
Created By   : Chiranjeevi Gogulakonda
Company Name : NTT Data
Project      : LCBS
Created Date : 
Test Class   : LCBSManageUsersController_Test
Test Class URL:https://ssoazure--devlcbs.cs17.my.salesforce.com/setup/build/viewApexClass.apexp?id=01pg0000000Gony 
Usages       : 
Modified By  :
***********************************/
public without sharing class LCBSManageUsersController {   
    public Contact contactEdit {get; set;}
    public Contact Contactinfo { get; set; }
    Public String ContactEmail {get;set;} 
    Public String CompanyLocation {get;set;}  
    Public String firstName{get;set;} 
    Public String lastName{get;set;}
    Public String phoneNumber{get;set;}  
    Public String Role {get;set;}   
    Public String timeZone{get;set;}       
    public List<Messaging.SingleEmailMessage> mails {get;set;}
    public List<String> Roles {get;set;}   
    Public list<Building__c> BuildList{set;get;}
    public List<User> users {get;set;}
    public List<Account> accnt {get;set;}
    public user u {get;set;}  
    public String PAGE_CSS { get; set; }


    public LCBSManageUsersController ()
    {
    PAGE_CSS = LCBSParameters.PAGE_CSS_URL;
     users = new List<User>();
    u = [Select Id,Contact.AccountId,Contact.FirstName,Contact.Lastname from User where ID =:UserInfo.getUserId()];
    accnt = new List<Account>();
    accnt = [Select Id, Name,(Select FirstName,LastName from Contacts),Address__c,City__c,Phone,Email__c FROM Account where ID IN (Select AccountToId from Partner where AccountFromId =:u.Contact.AccountId)];
          
        ContactEmail ='' ;
        system.debug('>>>>>>>>>>>?????'+this.ContactEmail);
        Contactinfo = new Contact();
        String conId = ApexPages.currentPage().getParameters().get('id');                            
        if ( conId != null ) { 
            contactEdit = 
                [select id,FirstName,LastName,Phone,Email,OwnerID,Role__c,Contractor_Company_Location__r.name
                 from Contact
                 where Id = :conId ];                                   
        }          
        if ( conId != null ) { 
            BuildList = [Select Id,Name,Address__c,Address_1__c,Address_2__c,Alternate_Dispatcher__c,Alternate_Technician__c,Building_Owner__c,City__c,Contractor_Company_Location__c,Country__c,Contractor_Company_Location__r.name,
                         Dispatcher__c,Building_Owner__r.name,Email_Address__c,First_Name__c,Last_Name__c,Number_of_Devices__c,Dispatcher__r.name,Technician__r.name,Phone_Number__c,Postal_Code__c,Site__c,State__c,Technician__c,Time_Zone__c,Title__c from Building__c where (Alternate_Dispatcher__c   =: conId) or (Dispatcher__c =: conId ) or (Technician__c =: conId) or (Alternate_Technician__c =:conId) ] ;           
            
        }        
    }    
    public List<Contact> ContactList {        
        get {
            if (ContactList == null) {
                ContactList = [select id,AccountId,FirstName,LastName,Phone,Email,OwnerID,Contractor_Company_Location__c,Role__c,Contractor_Company_Location__r.name
                               from Contact where AccountId in : accnt ]; // where id =:u.contact.id
            }
            return ContactList ;
        }
        set;
    }     
    public PageReference outboundEmail() {       
        try{            
            system.debug('Nagarajan ContactEmail'+ContactEmail);  
              Contact cnt = new   Contact();
              cnt.FirstName = firstName;
              cnt.LastName = lastName;
              cnt.Role__c = Role ;
              cnt.Contractor_Company_Location__c = CompanyLocation ;
              cnt.Phone = phoneNumber ;
              cnt.Email = ContactEmail ;
              cnt.Time_Zone__c = timeZone ;              
              insert cnt ;                                                                 
            mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            List<String> sendTo = new List<String>();
            sendTo.add(ContactEmail);
            system.debug('sendTo '+sendTo );
            mail.setToAddresses(sendTo);
            mail.setSenderDisplayName('LCBS User Registration');
            mail.setSubject('LCBS User Registration');           
           // string emailMessage ='Please use below link for User Registration. <br/> https://azure.microsoft.com/en-us/ <br/><br/> Thank you';    
            string emailMessage = '"This is to confirm that you have been added to your company’s LCBS remote services account".<br/> This email will eventually link to the dispatcher or technician view of their profile, where they could edit it.<br/><br/> Thank you';        
            mail.setHtmlBody(emailMessage);
            mails.add(mail);   
            Messaging.sendEmail(mails);
            System.debug('End of the mail');                                 
        }
        catch(DmlException e){ 
         String error = e.getMessage(); 
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,error));
             }
        PageReference p = new PageReference('/apex/LCBSManageUsers');
        p.setRedirect(true);
        return p;
    }    
    public PageReference updateUser() {
        try{
            update contactEdit;
        }
        catch(DmlException e){
            String error = e.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,error));
        }
        PageReference p = new PageReference('/apex/LCBSManageUsers');
        p.setRedirect(true);
        return p;
    }   
    public PageReference cancelUser() {
        PageReference p = new PageReference('/apex/LCBSManageUsers');
        p.setRedirect(true);
        return p;
    }           
    //Map<String,ID> profiles = new Map<String,ID>();
     // Profile ps = [select id, name from Profile where  name = 'System Administrator'];
     // User admin = [SELECT Id FROM user WHERE profileid = : ps.id and Isactive = true limit 1];                      
    Public ID paramValue{set;get;} 
  //  @future
    public PageReference  DeactivateUser() {  
 // system.runas(admin){ 
    system.debug('Enter***********');
   // UserDeactive.dynamic(this.paramValue);   
    List<User> usersToUpdate = new List<User>();
    for(User u : [Select u.Id, u.IsActive,u.ContactId,u.IsPortalEnabled from User u where u.ContactId  =: this.paramValue]){
   // system.debug('Chiran Param Value:'+this.paramValue);
   // system.debug('Chiran User list:'+u);
    if(u.IsActive || u.IsPortalEnabled ){
        u.IsActive = false;
      //  u.IsPortalEnabled = false;
        usersToUpdate.add(u);
    }   
}

if (usersToUpdate.size()>0){
system.debug('Update List:'+usersToUpdate);
    update usersToUpdate;
}      
        PageReference p = new PageReference('/apex/LCBSManageUsers');
        p.setRedirect(true);
        return p;       
    }   
    public SelectOption[] getCompanyLocOptions() 
    {      
        SelectOption[] getCompanyLocNames= new SelectOption[]{};  
            getCompanyLocNames.add(new SelectOption('','--None--'));
        for (Company_Location__c location : [select id,Name from Company_Location__c]) 
        {  
            getCompanyLocNames.add(new SelectOption(location.Id,location.Name));             
        }        
        return getCompanyLocNames;         
    }
    public SelectOption[] getRoleoptions() 
    {       
        SelectOption[] getRoleNames= new SelectOption[]{};        
        getRoleNames.add(new SelectOption('','--None--'));        
         Schema.DescribeFieldResult fieldResult =Contact.Role__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
       for( Schema.PicklistEntry f : ple)
       {
          getRoleNames.add(new SelectOption(f.getLabel(), f.getValue()));
       }                            
        return getRoleNames;         
    }   
    public SelectOption[] getTimeZoneOptions() 
    {       
        SelectOption[] getRoleNames= new SelectOption[]{}; 
        getRoleNames.add(new SelectOption('','--None--'));       
        Schema.DescribeFieldResult fieldResult =Contact.Time_Zone__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();      
       for( Schema.PicklistEntry f : ple)
       {
          getRoleNames.add(new SelectOption(f.getLabel(), f.getValue()));
       }                               
        return getRoleNames;         
    }           
    public pagereference newUser(){        
        pagereference p = new pagereference('/apex/LCBSNewUser'); 
        p.setredirect(true);
        return p; 
    }    
}