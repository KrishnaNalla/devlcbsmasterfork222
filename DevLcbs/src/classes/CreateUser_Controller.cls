Public Class CreateUser_Controller{
public User UserRecord{get;set;}
public string FirstName{get;set;}

public string LastName{get;set;}

public string UserName{get;set;}

public string EmailId{get;set;}

public string UserPhone{get;set;}

public Boolean Contractor{get;set;}

public Boolean Distributor{get;set;}

    public CreateUser_Controller(){
        UserRecord = new user();
    }
    
    public pagereference AddUserDetails(){
        
        UserRecord.FirstName = FirstName;
        UserRecord.LastName= LastName;
        UserRecord.UserName= UserName;
        UserRecord.Email= EmailId;
        UserRecord.Phone= UserPhone;
        UserRecord.I_am_a_Contractor__c= Contractor;
        UserRecord.I_am_a_Distributor__c= Distributor;
        system.debug('user record++++'+UserRecord);
        insert UserRecord;
        return null;
    }
}