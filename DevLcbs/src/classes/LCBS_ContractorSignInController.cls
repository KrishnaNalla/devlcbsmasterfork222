/************************************
Name         : LCBS_ContractorSignInController
Created By   : Nagarajan Varadarajan
Company Name : NTT Data
Project      : LCBS
Created Date : May 5,2015
Test Class   : 
Test Class URL: 
Usages       : 
Modified By  :Chiranjeevi Gogulakonda
***********************************/
public Class LCBS_ContractorSignInController{
public LCBS_ContractorSignInController()
{

}
public PageReference forwardToStartPage() {
       // return Network.communitiesLanding();
       //  string RecordTypeContractor = [SELECT id from RecordType where Name ='ECC:Contractor Contacts'].Id;
       //  string RecordTypeDistributor = [SELECT id from RecordType where Name ='ECC:Distributor Contacts'].Id;
        User u = [Select id, Contact.Name,Contact.RecordTypeId,contact.AccountId,contact.Account.EnvironmentalAccountType__c,contact.Role__c,contact.Account.EULA_Accepted__c from user where id=:userinfo.getUserId()];
        system.debug('######## Test Account type:'+u.contact.Account.EnvironmentalAccountType__c);
        system.debug('######## Naga Test:Before Record Type IF'+ u.contact.AccountId);
        if(u.contact.Account.EnvironmentalAccountType__c == 'Contractor' && u.contact.Account.EULA_Accepted__c == true && u.Contact.Role__c == 'Owner') //  && u.contact.Account.EULA_Accepted__c == true
        {
           system.debug('######## Test Account type:'+u.contact.Account.EnvironmentalAccountType__c);
           return Page.LCBS_AllBuildings;
        }
        else
        {
         ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'You are not a Contractor User or Your Account is not yet Approved. Please Try Again');
         ApexPages.addMessage(myMsg);
         //return Page.LCBS_Index;
         return null;
        }
    }

}