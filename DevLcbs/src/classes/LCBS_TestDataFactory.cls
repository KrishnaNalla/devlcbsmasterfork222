/************************************
Name         : LCBS_TestDataFactory
Created By   : Nagarajan Varadarajan
Company Name : NTT Data
Project      : LCBS
Created Date : 
Test Class   : 
Test Class URL: 
Usages       : 
Modified By  : Chiranjeevi Gogulakonda
***********************************/
public abstract class LCBS_TestDataFactory{
 public static List<Account> createAccounts( Integer numberOfAccounts) {

        List<Account> accounts = new List<Account>();

        for ( Integer i = 0 ; i < numberOfAccounts ; i++ ) {
             Account account = new Account( name = 'Test Account' + Math.random(), 
              Phone= '(415) 419-8873',ShippingStreet = '5353 W.Test Rd',
                 ShippingCity = 'Testdale', ShippingState = 'CA', ShippingPostalCode = '94803',Email__c ='abx@gmail.com');
                 accounts.add(account);
                }
        return accounts;
            }
            
                     
     public static List<Contact> createContact( Integer numberOfContact) {

        List<Contact> contacts= new List<Contact>();

        for ( Integer i = 0 ; i < numberOfContact ; i++ ) {
             Contact contact = new Contact (LastName='Test' + Math.random(), 
              Phone= '(415) 419-8873', Email = 'test@nttdata.com');
              insert contact;
                 contacts.add(contact);
                }
        return contacts;
            }   
            
            
            
            public static List<Contact> createContactDummy( Integer numberOfContact) {

        List<Contact> contacts= new List<Contact>();

        for ( Integer i = 0 ; i < numberOfContact ; i++ ) {
             Contact contact = new Contact (LastName='Test' + Math.random(), 
              Phone= '(415) 419-8873', Email = 'test@test.com');
              insert contact;
                 contacts.add(contact);
                }
        return contacts;
            }    
             
           
      /*  public static List<Contact> updateContact( Integer numberOfContact) {

        List<Contact> contacts= new List<Contact>();

        for ( Integer i = 0 ; i < numberOfContact ; i++ ) {
             Contact contact = new Contact (LastName='Test' + Math.random(), 
              Phone= '(415) 419-8873');
                 contacts.add(contact);
                }
        return contacts;
            }   
             */      
               
            
            public static List<Company_Location__c> createcompanyLocation( Integer numberOfcompanyLocation) {

        List<Company_Location__c> companyLocation = new List<Company_Location__c>();
             Account account = new Account( name = 'Test Account'+ Math.random(), 
              Phone= '(415) 419-8873',ShippingStreet = '5353 W.Test Rd',
                 ShippingCity = 'Testdale', ShippingState = 'CA', ShippingPostalCode = '94803');
                 insert account;
        for ( Integer i = 0 ; i < numberOfcompanyLocation; i++ ) {
             Company_Location__c loc = new Company_Location__c( name = 'Test Location' + Math.random(), 
              Address__c= 'test address',Country__c='United States',Email_Address__c='test@test.com',Company_Name__c =account.Id,
                 City__c = 'Testdale', State__c = 'ALABAMA', Postal_Code__c= '94803');
                 companyLocation.add(loc);
                }
        return companyLocation;
            }
            
        public static List<Building_Owner__c> createBuildingOwner( Integer numberOfBuildingOwners) {

        List<Building_Owner__c> BuildingOwner = new List<Building_Owner__c>();
             Account account = new Account( name = 'Test Account'+ Math.random(), 
              Phone= '(415) 419-8873',ShippingStreet = '5353 W.Test Rd',
                 ShippingCity = 'Testdale', ShippingState = 'CA', ShippingPostalCode = '94803');
                 insert account;
       
             Company_Location__c loc = new Company_Location__c( name = 'Test Location' + Math.random(), 
              Address__c= 'test address',Country__c='United States',Email_Address__c='test@test.com',Company_Name__c =account.Id,
                 City__c = 'Testdale', State__c = 'ALABAMA', Postal_Code__c= '94803');
                insert loc;
              Contractor_Branch_Location__c cb = new Contractor_Branch_Location__c( name='test',Account__c=account.id);  
              insert cb;
            for ( Integer i = 0 ; i < numberOfBuildingOwners; i++ ) { 
             Building_Owner__c b = new Building_Owner__c( Name='Test', Address_1__c='123', Address_2__c='456', City__c='Newyork',Company_Company_Location12__c = loc.id, Contractor_Branch_Location__c = cb.id ,Email_Address__c = 'test@nttdata.com');
             insert b;
             BuildingOwner.add(b);
       }
       
        return BuildingOwner;                 
        }   
        
                      
         public static List<Building__c> createBuilding( Integer numberOfBuilding) {

            List<Building__c> Building = new List<Building__c>();
             Account account = new Account( name = 'Test Account'+ Math.random(), 
              Phone= '(415) 419-8873',ShippingStreet = '5353 W.Test Rd',
                 ShippingCity = 'Testdale', ShippingState = 'CA', ShippingPostalCode = '94803',Email__c = 'abc@gmail.com');
                 insert account;
       
             Company_Location__c loc = new Company_Location__c( name = 'Test Location' + Math.random(), 
              Address__c= 'test address',Country__c='United States',Email_Address__c='test@test.com',Company_Name__c =account.Id,
                 City__c = 'Testdale', State__c = 'ALABAMA', Postal_Code__c= '94803');
                insert loc;
                
              Site__c osite = new Site__c(name='test');
              insert osite ;
                
              Contractor_Branch_Location__c cb = new Contractor_Branch_Location__c( name='test',Account__c=account.id);  
              insert cb;
              
            Building_Owner__c buildow  = new Building_Owner__c(Name='Test ', Address_1__c='123 ', Address_2__c='456 ',Address__c ='123 cross street', City__c='Newyork',Email_Address__c='test@gmail.com',Company_Company_Location12__c = loc.id, Contractor_Branch_Location__c = cb.id );
            insert buildow ; 
            
            Contact con = new Contact (LastName='Test');
            insert con;
            
            
            for ( Integer i = 0 ; i < numberOfBuilding; i++ ) { 
             Building__c bu = new Building__c( Name='Test', Address_1__c='123',Email_Address__c='test@test.com', Address_2__c='456',Address__c ='123 cross street',Country__c='United States', City__c='Newyork',Contractor_Company_Location__c= loc.id,First_Name__c = 'jake',State__c = 'ALABAMA',Last_Name__c = 'john',Building_Owner__c=buildow.id, Site__c=osite.id,Number_of_Devices__c=2,Phone_Number__c = '9087678906',Title__c = 'mr.',Time_Zone__c = '(GMT+14:00) Line Islands Time (Pacific/Kiritimati)',Technician__c=con.id,Postal_Code__c= '94803',Dispatcher__c=con.id,Alternate_Technician__c=con.id,Alternate_Dispatcher__c=con.id);
             insert bu;
             Building.add(bu);
       }
       
        return Building;                 
        }      
         
                  
        }