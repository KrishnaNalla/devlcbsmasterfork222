public class LCBS_ContractorInviteDelegate_Controller
{
public static id contractor {get;set;} 
public static id distributor {get;set;} 
public contact cc {get;set;}
public contact invitedContact {get;set;}
public String PAGE_CSS { get; set; }
public user u {get;set;}
public Account c {get;set;}
public string orgid {get;set;}
public document logo {get;set;}
public document sign {get;set;}
public string sfdcBaseURL{get;set;}
String Env;
public string executive_name {get;set;}
public string executive_phone {get;set;}
public string executive_email {get;set;}
public string url {get;set;}
public string userName {get;set;}
public string password {get;set;}
public boolean conTerms{get;set;}
public boolean distTerms{get;set;}
public String d {get;set;}
 public LCBS_ContractorInviteDelegate_Controller()
 {
  d= DateTime.now().format('MMMMM dd, yyyy');
  sfdcBaseURL = System.URL.getSalesforceBaseURL().toExternalForm();
  orgid= UserInfo.getOrganizationId();
  logo = [select id,name from document where name='LCBS_Email_Logo']; 
  sign = [select id,name from document where name='Honeywell vicepresident sign']; 
  cc = [select id,name,email,LCBS_UserName__c,Role__c,LCBS_Password__c,LCBS_FedId__c,firstname,lastname,Account.Name,Account.EnvironmentalAccountType__c,Account.Email__c,account.Phone,Account.Account_Executive_Info__c,Account.Account_Exec_Name__c,Account.Account_Exec_Phone__c,Account.Account_Exec_Email__c from contact where id=:contractor];
  if(cc.LCBS_UserName__c !=NULL)
  {
   userName = cc.LCBS_UserName__c;
  }
  else
  {
   userName = '';
  }
  if(cc.LCBS_Password__c != NULL)
  {
   password = cc.LCBS_Password__c;
  }
  else
  {
   password = '';
  }
  if(cc.Account.EnvironmentalAccountType__c == 'Distributor'){
  distTerms = true;
  }
  if(cc.Account.EnvironmentalAccountType__c == 'Contractor'){
  conTerms = true;
  }
  
  password = cc.LCBS_Password__c;
  system.debug('Invited Employee Contact'+cc);
  invitedContact =[select id,name,Email from contact where id=:distributor];
  // c = [select id,name,(select id,firstname,lastname,email,phone from contacts) from Account where id=:system.label.Honeywell_Account_Executive];
   //system.debug('%%'+c.contacts[0].firstname);
          if(cc.Account.Account_Executive_Info__c !=null)
           {
            //String[] exec = cc.Account.Account_Executive_Info__c.split('\\|');
          //  for(String c: exec)
           // {
             
             executive_name = cc.Account.Account_Exec_Name__c;
             executive_email =cc.Account.Account_Exec_Email__c;
             executive_phone =cc.Account.Account_Exec_Phone__c;
             }
             
             else
             {
              executive_name ='';
              executive_email ='';
              executive_phone ='';
             }
  //PAGE_CSS = LCBSParameters.PAGE_CSS_URL;
 }

}