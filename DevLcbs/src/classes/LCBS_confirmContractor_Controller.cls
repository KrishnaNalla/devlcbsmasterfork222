/************************************
Name         : LCBS_confirmContractor_Controller
Created By   : Nagarajan Varadarajan
Company Name : NTT Data
Project      : LCBS
Created Date : 
Test Class   : 
Test Class URL: 
Usages       : 
Modified By  :
***********************************/

public class LCBS_confirmContractor_Controller{
    
    public Contact fetchaccinfo{get;set;}
    public String currentRecordId {get;set;}
    public boolean displayPopup {get; set;}
    public string executive_name {get;set;}
    public string executive_email {get;set;}
    public string executive_phone {get;set;}
    Public boolean polRenTab{get;set;}
    public Account c {get;set;}
    
    public LCBS_confirmContractor_Controller(){
        fetchaccinfo = new Contact();
        currentRecordId  = ApexPages.CurrentPage().getparameters().get('id');
        fetchaccinfo = [select id, Contractor_EULA_Accepted__c, Contractor_Eula_Rejected__c, Account.Account_Exec_Name__c, Account.Account_Exec_Email__c, Account.Account_Exec_Phone__c, Account.EnvironmentalAccountType__c, Date_EULA_Sent__c, LCBS_UserName__c, LCBS_Password__c, LCBS_FedId__c, FirstName, LastName, Email, phone from Contact where id =:currentRecordId];
        // Account.Account_Executive_Info__c
        system.debug('*****fetchinfo '+fetchaccinfo);
        // c = [select id,name,(select id,firstname,lastname,email,phone from contacts) from Account where id=:system.label.Honeywell_Account_Executive];
        //system.debug('%%'+c.contacts[0].firstname);
        
         if(fetchaccinfo.Account.Account_Exec_Name__c != NULL){
            executive_name = fetchaccinfo.Account.Account_Exec_Name__c;
        }
        else{
            executive_name = '';
        }
        
        if(fetchaccinfo.Account.Account_Exec_Email__c != NULL){
            executive_email = fetchaccinfo.Account.Account_Exec_Email__c;
        }
        else{
            executive_email = '';
        }
        
        if(fetchaccinfo.Account.Account_Exec_Phone__c != NULL){
            executive_phone = fetchaccinfo.Account.Account_Exec_Phone__c;
        }
        else{
            executive_phone = '';
        }
        
        /* if(fetchaccinfo.Account.Account_Executive_Info__c !=null){
            // String[] exec = fetchaccinfo.Account.Account_Executive_Info__c.split('\\|');
            executive_name = fetchaccinfo.Account.Account_Exec_Name__c;
            executive_email = fetchaccinfo.Account.Account_Exec_Email__c;
            executive_phone = fetchaccinfo.Account.Account_Exec_Phone__c;
        }
        else{
            executive_name = '';
            executive_email = '';
            executive_phone = '';
        } */
       
    }
    
    public void closePopup(){
        displayPopup = false;
    }
    public void showPopup(){
        displayPopup = true;
    }
    public boolean isContractor{
       get {
           if(fetchaccinfo.Account.EnvironmentalAccountType__c == 'Contractor')
           { 
               return true;
           }
           else
           { 
               return false;
           }
       }
       set;
       }
       public boolean isDistributor{
       get {
           if(fetchaccinfo.Account.EnvironmentalAccountType__c == 'Distributor')
           { 
               return true;
           }
           else
           { 
               return false;
           }
       }
       set;
       }
     public boolean Approved {
       get {
           if(fetchaccinfo.Contractor_EULA_Accepted__c== true)
           { 
               return true;
           }
           else
           { 
               return false;
           }
       }
       set;
       }
       
       public boolean eulaexpired{
       get {
           Integer noOfDays = fetchaccinfo.Date_EULA_Sent__c.daysBetween(Date.today());
           system.debug('Naga Test' + noOfDays );
           if((noOfDays  > 2) && (fetchaccinfo.Contractor_EULA_Accepted__c== FALSE))
           { 
               return true;
           }
           else
           { 
               return false;
           }
       }
       set;
       }
      
     public PageReference approveAction()
        {
          string url = system.label.LCBSUserResponseURL+'?id='+currentRecordId ;
            try
            {
            system.debug('*****fetchinfo '+fetchaccinfo.id); 
            LCBSAddUserCallout.onAfterEulaAccept(fetchaccinfo.id);
            fetchaccinfo.Contractor_EULA_Accepted__c =True;
            fetchaccinfo.Date_EULA_Accepted__c =system.today();
            update fetchaccinfo;
            }
            catch (DMLException ex){
            system.debug(ex.getMessage());
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'Account does not exists');
            ApexPages.addMessage(msg);
            
            Approved = false;
             } 
          PageReference pr = new PageReference(url);
          return pr;
        }
        
       public boolean Rejected{
       get {
           if(fetchaccinfo.Contractor_Eula_Rejected__c  == true)
           { 
               return true;
           }
           else
           { 
               return false;
           }
       }
       set;
       }
        
      public void rejectAction() {
        displayPopup = false;
       try
            {
            system.debug('*****fetchinfo***** '+fetchaccinfo); 
            fetchaccinfo.Contractor_Eula_Rejected__c  =True;
             fetchaccinfo.Contractor_Invited__c = false;
             fetchaccinfo.Date_EULA_Rejected__c = system.today();
            update fetchaccinfo;
            }
            catch (DMLException ex){
            system.debug(ex.getMessage());
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'Account does not exists');
            ApexPages.addMessage(msg);
            
            Rejected= false;
             }
        
    }
    
    

}